#!/usr/bin/python2
# coding: utf-8

import os
import time
import sys
import shutil

catalog = sys.argv[1]

for root, dirs, files in os.walk(catalog):
    for filename in files:  # przeglądamy rekursywnie wszystkie pliki w katalogu podanym, jako pierwszy argument w linii komend
        if "obci.raw" in filename and "2017" not in filename and "z" not in filename and "scene" not in filename:  # szukając plików *obci.raw
            filepath = os.path.join(root, filename)
            
            base_filename = filename[:-9]
            print base_filename
            
            
            obciraw_path =  os.path.join(root, base_filename + ".obci.raw")
            etrraw_path =  os.path.join(root, base_filename + ".etr.raw")
            obcitag_path = os.path.join(root, base_filename + ".obci.tag")
            obcixml_path = os.path.join(root, base_filename + ".obci.xml")
            etrxml_path = os.path.join(root, base_filename + ".etr.xml")
            
            
            patient_name = os.path.split(root)[-1]
            if len(patient_name) != 2:
                patient_name = os.path.split(os.path.split(root)[-2])[-1]
            print 'patient name:', patient_name

            
            file_size = os.path.getsize(filepath)
            
            if file_size > 1.7e8:  # różnicuję po wielkości, scena 6 ma około 100 MB, a sceny 1-5 jakieś 200 do 250 MB, więc dyskryminuję na 170 MB
                scene = "s1-5"
            else:
                scene = "6"
            
            date = time.strftime("%Y%m%dz%H%M%S", time.gmtime(os.path.getmtime(filepath)))  # getctime reaguje nawet na zmianę nazwy i innych metadanych pliku,
                                                                                            # mtime tylko na zmianę zawartości
            
            new_base_filename = "{}_scene{}_{}".format(patient_name, scene, date)
            
            
            obciraw_newpath =  os.path.join(root, new_base_filename + ".obci.raw")
            etrraw_newpath = os.path.join(root, new_base_filename + ".etr.raw")
            obcitag_newpath = os.path.join(root, new_base_filename + ".obci.tag")
            obcixml_newpath = os.path.join(root, new_base_filename + ".obci.xml")
            etrxml_newpath = os.path.join(root, new_base_filename + ".etr.xml")




            print filepath

            shutil.move(obciraw_path, obciraw_newpath)
            shutil.move(etrraw_path, etrraw_newpath)
            shutil.move(obcitag_path, obcitag_newpath)
            shutil.move(obcixml_path, obcixml_newpath)
            shutil.move(etrxml_path, etrxml_newpath)
