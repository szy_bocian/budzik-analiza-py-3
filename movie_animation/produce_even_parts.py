#!/usr/bin/python3

from project_settings import before_playlist, after_playlist, playlist_start, playlist_end
from project_settings import entry_start, entry_inside, entry_end
from project_settings import lengths, producers_ids

import numpy.random as rnd
rnd.seed(42)


stages = ['"kaczka"', '"kaczka"', '"pies"', '"pies"']
target_counters = [18, 22, 23, 17]

min_target_separation = (2, 4)
max_target_separation = 0

tags = []

for stage_id, (stage, target_counter) in enumerate(zip(stages, target_counters)):
    playlist = ""
    last_producer = out = last_out = "spam"

    nontarget_counter = 2
    nontarget_global_counter = 0

    while target_counter:
        producer = producers_ids[rnd.randint(len(producers_ids))]
        if nontarget_counter < -max_target_separation:
            producer = stage
        while nontarget_counter > 0 and producer == stage or producer == last_producer:
            producer = producers_ids[rnd.randint(len(producers_ids))]
        if producer == stage:
            target_counter -= 1
            nontarget_counter = rnd.randint(*min_target_separation)
            producer = producer[:-1] + '-klatka"'
        else:
            nontarget_counter -= 1
            nontarget_global_counter += 1
        
        last_producer = producer
    
        while out == last_out:
            out = lengths[rnd.randint(len(lengths))]
        last_out = out
        
        entry = entry_start
        entry += producer
        entry += entry_inside
        entry += out
        entry += entry_end
        
        playlist += entry
        
        if '-klatka"' in producer:
            tag = producer[:-8].strip('"') + '\t' + "target\n"
        else:
            tag = producer.strip('"') + '\t' + "nontarget\n"
        tags.append(tag)

    
    text = before_playlist + playlist_start + playlist + playlist_end + after_playlist
    
    mlt_file = open("sc06_v2_part{}.mlt".format(2+2*stage_id), "w")
    mlt_file.writelines(text)
    
    tags_file = open("scena_6", "w")
    tags_file.writelines(tags)
    
    print(stage, "nontargets", nontarget_global_counter / target_counters[stage_id])
    print(stage, "nontargets", nontarget_global_counter)
