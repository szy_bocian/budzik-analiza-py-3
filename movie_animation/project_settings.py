before_playlist = """<?xml version="1.0" encoding="utf-8"?>
<mlt LC_NUMERIC="pl_PL.UTF-8" version="6.5.0" title="Shotcut version 17.09.04" producer="main bin">
  <profile description="automatic" width="1920" height="1080" progressive="1" sample_aspect_num="1" sample_aspect_den="1" display_aspect_num="1920" display_aspect_den="1080" frame_rate_num="60" frame_rate_den="1" colorspace="709"/>
  <producer id="kaczka-klatka" title="Anonymous Submission" in="00:00:00,000" out="00:00:02,017">
    <property name="length">00:00:02,033</property>
    <property name="eof">pause</property>
    <property name="resource">kaczka-klatka.mp4</property>
    <property name="audio_index">1</property>
    <property name="video_index">0</property>
    <property name="mute_on_pause">0</property>
    <property name="mlt_service">avformat-novalidate</property>
    <property name="seekable">1</property>
    <property name="aspect_ratio">1</property>
    <property name="global_feed">1</property>
    <property name="xml">was here</property>
    <property name="shotcut:hash">75d00e92da490f5bc91f3e16c556c916</property>
    <property name="shotcut:caption">kaczka-klatka.mp4</property>
    <property name="shotcut:detail">kaczka-klatka.mp4</property>
    <property name="shotcut:comment"></property>
  </producer>
  <producer id="pies" title="Anonymous Submission" in="00:00:00,000" out="00:00:02,017">
    <property name="length">00:00:02,033</property>
    <property name="eof">pause</property>
    <property name="resource">pies.mp4</property>
    <property name="audio_index">1</property>
    <property name="video_index">0</property>
    <property name="mute_on_pause">0</property>
    <property name="mlt_service">avformat-novalidate</property>
    <property name="seekable">1</property>
    <property name="aspect_ratio">1</property>
    <property name="global_feed">1</property>
    <property name="xml">was here</property>
    <property name="shotcut:hash">7c81a7088c01b8907bc676498b433524</property>
  </producer>
  <producer id="pies-klatka" title="Anonymous Submission" in="00:00:00,000" out="00:00:02,017">
    <property name="length">00:00:02,033</property>
    <property name="eof">pause</property>
    <property name="resource">pies-klatka.mp4</property>
    <property name="audio_index">1</property>
    <property name="video_index">0</property>
    <property name="mute_on_pause">0</property>
    <property name="mlt_service">avformat-novalidate</property>
    <property name="seekable">1</property>
    <property name="aspect_ratio">1</property>
    <property name="global_feed">1</property>
    <property name="xml">was here</property>
    <property name="shotcut:hash">aa1c3350e51c81f8276ed62bc8199598</property>
  </producer>
  <producer id="sowa" title="Anonymous Submission" in="00:00:00,000" out="00:00:02,017">
    <property name="length">00:00:02,033</property>
    <property name="eof">pause</property>
    <property name="resource">sowa.mp4</property>
    <property name="audio_index">1</property>
    <property name="video_index">0</property>
    <property name="mute_on_pause">0</property>
    <property name="mlt_service">avformat-novalidate</property>
    <property name="seekable">1</property>
    <property name="aspect_ratio">1</property>
    <property name="global_feed">1</property>
    <property name="xml">was here</property>
    <property name="shotcut:hash">dc2389045e455f37101fb45f79d3c2c8</property>
  </producer>
  <producer id="zaba" title="Anonymous Submission" in="00:00:00,000" out="00:00:02,017">
    <property name="length">00:00:02,033</property>
    <property name="eof">pause</property>
    <property name="resource">zaba.mp4</property>
    <property name="audio_index">1</property>
    <property name="video_index">0</property>
    <property name="mute_on_pause">0</property>
    <property name="mlt_service">avformat-novalidate</property>
    <property name="seekable">1</property>
    <property name="aspect_ratio">1</property>
    <property name="global_feed">1</property>
    <property name="xml">was here</property>
    <property name="shotcut:hash">7abaa0357e0839d1750bcc819361041a</property>
  </producer>
  <producer id="kaczka" title="Anonymous Submission" in="00:00:00,000" out="00:00:02,017">
    <property name="length">00:00:02,033</property>
    <property name="eof">pause</property>
    <property name="resource">kaczka.mp4</property>
    <property name="audio_index">1</property>
    <property name="video_index">0</property>
    <property name="mute_on_pause">0</property>
    <property name="mlt_service">avformat-novalidate</property>
    <property name="seekable">1</property>
    <property name="aspect_ratio">1</property>
    <property name="shotcut:hash">556f44a94209808c5ff07af989474403</property>
    <property name="ignore_points">0</property>
    <property name="shotcut:caption">kaczka.mp4</property>
    <property name="shotcut:detail">kaczka.mp4</property>
    <property name="shotcut:comment"></property>
    <property name="global_feed">1</property>
    <property name="xml">was here</property>
  </producer>
  <playlist id="main bin">
    <property name="xml_retain">1</property>
    <entry producer="kaczka-klatka" in="00:00:00,000" out="00:00:02,017"/>
    <entry producer="pies" in="00:00:00,000" out="00:00:02,017"/>
    <entry producer="pies-klatka" in="00:00:00,000" out="00:00:02,017"/>
    <entry producer="sowa" in="00:00:00,000" out="00:00:02,017"/>
    <entry producer="zaba" in="00:00:00,000" out="00:00:02,017"/>
    <entry producer="kaczka" in="00:00:00,000" out="00:00:02,017"/>
  </playlist>
  <producer id="black" in="00:00:00,000" out="00:00:28,883">
    <property name="length">00:00:28,900</property>
    <property name="eof">pause</property>
    <property name="resource">black</property>
    <property name="aspect_ratio">1</property>
    <property name="mlt_service">color</property>
    <property name="set.test_audio">0</property>
  </producer>
  <playlist id="background">
    <entry producer="black" in="00:00:00,000" out="00:00:28,883"/>
  </playlist>
"""

after_playlist = """<tractor id="tractor0" title="Shotcut version 17.09.04" global_feed="1" in="00:00:00,000" out="00:00:28,883">
    <property name="shotcut">1</property>
    <property name="shotcut:scaleFactor">6,17204</property>
    <track producer="background"/>
    <track producer="playlist0"/>
    <transition id="transition0">
      <property name="a_track">0</property>
      <property name="b_track">1</property>
      <property name="mlt_service">mix</property>
      <property name="always_active">1</property>
      <property name="sum">1</property>
    </transition>
    <transition id="transition1">
      <property name="a_track">0</property>
      <property name="b_track">1</property>
      <property name="version">0,9</property>
      <property name="mlt_service">frei0r.cairoblend</property>
      <property name="disable">1</property>
    </transition>
  </tractor>
</mlt>
"""

playlist_start = """<playlist id="playlist0">
    <property name="shotcut:video">1</property>
    <property name="shotcut:name">V1</property>
"""

entry_start = "<entry producer="
entry_inside = ' in="00:00:00,000" out='
entry_end = "/>\n"

playlist_end = "</playlist>\n"

lengths = ['"00:00:02,000"',
           '"00:00:01,983"', '"00:00:01,967"', '"00:00:01,950"', '"00:00:01,933"', '"00:00:01,917"', '"00:00:01,900"',
           '"00:00:01,883"', '"00:00:01,867"', '"00:00:01,850"', '"00:00:01,833"', '"00:00:01,817"', '"00:00:01,800"',
           '"00:00:01,783"', '"00:00:01,767"', '"00:00:01,750"', '"00:00:01,733"', '"00:00:01,717"', '"00:00:01,700"',
           '"00:00:01,683"', '"00:00:01,667"', '"00:00:01,650"', '"00:00:01,633"', '"00:00:01,617"', '"00:00:01,600"',
           '"00:00:01,583"', '"00:00:01,567"', '"00:00:01,550"', '"00:00:01,533"', '"00:00:01,517"', '"00:00:01,500"',
           ]

lengths = ['"00:00:02,000"',
           '"00:00:01,967"', '"00:00:01,933"', '"00:00:01,900"',
           '"00:00:01,867"', '"00:00:01,833"', '"00:00:01,800"',
           '"00:00:01,767"', '"00:00:01,733"', '"00:00:01,700"',
           '"00:00:01,667"', '"00:00:01,633"', '"00:00:01,600"',
           '"00:00:01,567"', '"00:00:01,533"', '"00:00:01,500"',
           '"00:00:01,467"', '"00:00:01,433"', '"00:00:01,400"',
           ]

producers_ids = ['"kaczka"', '"pies"', '"sowa"', '"zaba"']