#!/usr/bin/env python2
# coding: utf8
from classify_score import boot_strap_aucs

# classifier always learns on 100-1, in the end on 100 and tested on remainder
# boot_strap_aucs(average=3, random_n=100, output_prefix='/repo/fizmed/coma/results/mdovgialo/result_archive/wyniki_ar/wyniki_AR_MP_r21_ears_200T_loo/downsample/wyniki_AR_MP_r21_ears_200T_loo', pipeline_type='downsampling', validation='loo')

# kfolds splits in 2, classifier always learns on 100

for i in range(100, 9, -10):
    boot_strap_aucs(average=1, first_n=i, output_prefix='/repo/fizmed/coma/results/mdovgialo/result_archive/wyniki_ar/wyniki_AR_MP_r21_ears_200T_1_to_100_global_classifier/downsample/wyniki_AR_MP_r21_ears_200T_2folds_global_classifier', pipeline_type='downsampling', validation='kfold', use_additional_data=False)
# boot_strap_aucs(average=1, output_prefix='/repo/fizmed/coma/results/mdovgialo/result_archive/wyniki_ar/wyniki_AR_MP_r21_ears_200T_2folds/downsample/wyniki_AR_MP_r21_ears_200T_2folds', pipeline_type='downsampling', validation='kfold')


# boot_strap_aucs(average=1, random_n=100, output_prefix='/repo/fizmed/coma/results/mdovgialo/result_archive/wyniki_ar/wyniki_AR_MP_r21_ears_200T_loo/downsample/wyniki_AR_MP_r21_ears_200T_loo', pipeline_type='downsampling', validation='loo')
