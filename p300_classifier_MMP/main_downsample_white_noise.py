#!/usr/bin/env python2
# coding: utf8
from classify_score import boot_strap_aucs

boot_strap_aucs(average=3, output_prefix='/repo/fizmed/coma/results/mdovgialo/result_archive/wyniki_szum/bialyszum_no_shrinkage_200T_2folds/downsample/bialyszum_200T_2folds', pipeline_type='downsampling', validation='kfold')
boot_strap_aucs(average=1, output_prefix='/repo/fizmed/coma/results/mdovgialo/result_archive/wyniki_szum/bialyszum_no_shrinkage_200T_2folds/downsample/bialyszum_200T_2folds', pipeline_type='downsampling', validation='kfold')