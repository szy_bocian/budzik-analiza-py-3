# coding: utf-8
import numpy as np
import sklearn
from scipy.stats import mannwhitneyu
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.model_selection import StratifiedKFold
import pylab as pb
import scipy.stats

from tqdm import tqdm

N_pat = 1000
N_tgt = 200
N_ntgt = 200
N_features = 24*16



classes = np.zeros(N_ntgt+N_tgt, dtype=int)
classes[:N_tgt] = 1

aucs = []
p_vals = []

stds_target = []
stds_ntarget = []

stds_greater = []
stds_lower = []

rank_fraction = []


for patient in xrange(N_pat):
    data = np.random.random((N_tgt + N_ntgt, N_features))
    skf = StratifiedKFold(n_splits=2)

    real_cls = []
    predict = []

    clf = LinearDiscriminantAnalysis(solver='lsqr', shrinkage='auto')
    for train, test in skf.split(data, classes):
        clf.fit(data[train], classes[train])
        predict_fold = clf.predict_proba(data[test])[:, 1]
        classes_fold = classes[test]
        real_cls.extend(classes_fold)
        predict.extend(predict_fold)

    real_cls = np.array(real_cls)
    predict = np.array(predict)

    bins = np.linspace(0, 1, 50)

    ntgt_preds = predict[real_cls==0]
    tgt_preds = predict[real_cls==1]

    # fig = pb.figure()
    # pb.hist(ntgt_preds, color='red', alpha=0.3, bins=bins)
    # pb.hist(tgt_preds, color='green', alpha=0.3, bins=bins)
    # pb.title("Decisions")
    # pb.xlim([0, 1])
    #
    # pb.savefig('img/patient_{}.png'.format(str(patient).rjust(3, '0')))
    # pb.close(fig)

    # histogram std ntgt_preds tgt_preds po pacjentach
    # histogram większego i mniejszego std decyzji

    tgt_std = np.std(tgt_preds)
    ntgt_std = np.std(ntgt_preds)

    greater_std = max((tgt_std, ntgt_std))
    less_std = min((tgt_std, ntgt_std))

    stds_target.append(tgt_std)
    stds_ntarget.append(ntgt_std)
    stds_greater.append(greater_std)
    stds_lower.append(less_std)

    auc = sklearn.metrics.roc_auc_score(real_cls, predict)
    U, p_val = mannwhitneyu(tgt_preds, ntgt_preds, alternative='less')
    aucs.append(auc)
    p_vals.append(p_val)

    same_predict = (len(predict)/len(np.unique(predict)))
    rank_fraction.append(same_predict)



fig = pb.figure()
pb.hist(p_vals, bins=bins)
pb.title("P_vals 5 percentile {}".format(scipy.stats.scoreatpercentile(p_vals, 5)))
pb.xlim([0, 1])

pb.savefig('img/_pvals.png.png')
pb.close(fig)

fig = pb.figure()
pb.hist(aucs, bins=bins)
pb.title("AUCS 5 percentile: {}".format(scipy.stats.scoreatpercentile(aucs, 95)))
pb.xlim([0, 1])

pb.savefig('img/_aucs.png')
pb.close(fig)

fig = pb.figure()
pb.hist(stds_ntarget, bins=40, histtype='step')
pb.hist(stds_target, bins=40, histtype='step')
pb.title("STD_nontarget vs target")

pb.savefig('img/_std_ntarget.png')
pb.close(fig)

fig = pb.figure()
pb.hist(stds_greater, bins=40, histtype='step')
pb.hist(stds_lower, bins=40, histtype='step')
pb.title("STD_greater")

pb.savefig('img/_std_greater_less.png')
pb.close(fig)

fig = pb.figure()
pb.hist(rank_fraction, bins=40, histtype='step')
pb.title("rank_fraction")

pb.savefig('img/_rank_fraction.png')
pb.close(fig)










