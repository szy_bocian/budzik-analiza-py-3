import matplotlib
matplotlib.use("Agg")
import os
from scipy.stats import sem

import pandas as pd
import sys
import pylab as pb

def sanitize_work_dir(work_dir):
    WORK_DIR = work_dir.rstrip(r'\/')
    if WORK_DIR.startswith('file:'):
        WORK_DIR = WORK_DIR[7:]
    return WORK_DIR

WORK = sys.argv[1]
WORK_DIR = sanitize_work_dir(WORK)

def draw_n_avr(work_dir):
    for dir in os.listdir(work_dir):
        if dir.startswith('average_n_'):
            n = int(dir.split('_')[2])

            database_path = os.path.join(work_dir, dir, '_sorted', 'results_database.csv')
            print dir, n, database_path
            database = pd.read_csv(database_path)

            database = database.query("measure=='AUC'")

            database['n_avr'] = [n] * database.shape[0]
            try:
                full_database = pd.concat((full_database, database))
            except NameError:
                full_database = database.copy()
    full_database.to_csv(os.path.join(work_dir, 'auc_max_full.csv'))

    full_database = full_database.rename(columns={'round': 'round_'})

    paradigms = full_database.paradigm.unique()
    for paradigm in paradigms:
        for diagnosis in full_database.diagnosis.unique():
            fig = pb.figure()
            ax = fig.add_subplot(111)
            patients = full_database.query('diagnosis=="{}"'.format(diagnosis)).id
            rounds = full_database.query('diagnosis=="{}"'.format(diagnosis))['round_']
            measurements = set(zip(patients, rounds))
            # import IPython
            # IPython.embed()
            for measurement in measurements:
                # import IPython
                # IPython.embed()
                patient_data = full_database.query('diagnosis=="{}"&id=="{}"&round_=={}&paradigm=="{}"'.format(diagnosis, measurement[0], measurement[1], paradigm))

                ax.plot(patient_data.n_avr.values, patient_data.value.values, label='{} r{}'.format(*measurement))
                ax.set_title('{} {} s'.format(diagnosis, paradigm))
            ax.legend()
            ax.set_ylim([0, 1])
            fig.savefig(os.path.join(work_dir, '{}_{}_s.png'.format(diagnosis, paradigm)))
            pb.close(fig)

            x_avr = []
            y_avr = []
            y_s = []
            for n_avr in full_database.n_avr.unique():
                data = full_database.query(
                    'diagnosis=="{}"&paradigm=="{}"&n_avr=={}'.format(diagnosis, paradigm, n_avr))
                x_avr.append(n_avr)

                # import IPython
                # IPython.embed()
                y_avr.append(data.value.mean())
                y_s.append(sem(data.value))

            fig = pb.figure()
            ax = fig.add_subplot(111)
            ax.errorbar(x_avr, y_avr, y_s)
            ax.set_title('{} {} avr'.format(diagnosis, paradigm))
            ax.set_ylim([0, 1])
            fig.savefig(os.path.join(work_dir, '{}_{}_avr.png'.format(diagnosis, paradigm)))
            pb.close(fig)





        # pb.show()

if __name__ == '__main__':
    draw_n_avr(WORK_DIR)
