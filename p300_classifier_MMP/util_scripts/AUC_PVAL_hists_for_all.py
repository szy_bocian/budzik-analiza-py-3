import sys
import os
from AUC_PVAL_hist import draw_aucs_p_vals_hists, sanitize_work_dir

WORK = sys.argv[1]
WORK_DIR = sanitize_work_dir(WORK)

CUTOFF_DEPTH = 3

result_database = os.path.join(WORK_DIR, 'result.csv')

HEADER = 'auc90;auc95;auc99;n_targets;n_nontargets;avr_type;ar_type;pipeline_type;n_patients'.split(';')

result_database_f = open(result_database, 'w')
result_database_f.write(','.join(HEADER))
result_database_f.write('\n')

for root, dirs, files in os.walk(WORK_DIR):
    if os.path.relpath(root, WORK_DIR).count(os.sep) == CUTOFF_DEPTH:
        del dirs[:]
        print root, dirs, files

        avr_type = root.split(os.sep)[-1]
        ar_type = root.split(os.sep)[-2]
        pipeline_type = root.split(os.sep)[-3]
        result_dict = draw_aucs_p_vals_hists(root)

        auc95 = result_dict['95_perc_auc']
        auc99 = result_dict['99_perc_auc']
        auc90 = result_dict['90_perc_auc']

        n_target = result_dict['mean_n_targets']
        n_nontarget = result_dict['mean_n_nontargets']
        n_patients = len(result_dict['aucs'])

        result_database_f.write(','.join([str(i) for i in [auc90,
                                                           auc95,
                                                           auc99,
                                                           n_target,
                                                           n_nontarget,
                                                           avr_type,
                                                           ar_type,
                                                           pipeline_type,
                                                           n_patients]]))
        result_database_f.write('\n')


result_database_f.close()