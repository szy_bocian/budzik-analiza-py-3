from mne.preprocessing import Xdawn
from mne.epochs import concatenate_epochs

class CustomXdawn(Xdawn):
    def transform(self, epochs):
        r = self.apply(epochs)

        key = r.keys()[0]

        return r[key]
