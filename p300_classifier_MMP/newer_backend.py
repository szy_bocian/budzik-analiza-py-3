#coding: utf8

import numpy as np
import os

import sys
from book_importer import read_book

from backend import get_template_distr, lda_test, testuj_rozklad, bayes_class, bayes_class_multi
import scipy.stats as st

import pylab as pb

NATOMS = 12



def hist_statistic(cuts_cor, cuts_incor, chnls):

    N_to_train = len(cuts_incor) / 2
    N_corr_train = len(cuts_cor)/2

    filename = cuts_cor.info["filename"]

    cuts_learn = cuts_incor[:N_to_train]
    mmp = 'MMP3'
    book_learn = cuts_matching_pursuit(cuts_learn, chnls, type='learn', mmp=mmp)
    cuts_learn = cuts_learn.get_data()*1e6
    cuts_test = cuts_incor[N_to_train:].get_data()*1e6

    #np.random.shuffle(cuts_cor)
    cuts_cor_train = cuts_cor[:N_corr_train].get_data()*1e6
    cuts_cor_test = cuts_cor[N_corr_train:].get_data()*1e6
    # ~ [[np.mean(cuts_learn, axis=0)[0]],]

    opt_amp = True
    opt_ph = True
    draw_pictures = False
    path_to_book = os.path.abspath(os.path.dirname(sys.argv[1]) + book_learn[2:-4] + '_mmp.b')
    data, signals, atoms, epoch_s = read_book(path_to_book)

    # ~ get_template_distr(cuts_learn, atoms, epoch_s, data, draw=False, mmp='MMP3', type='train_incor', opt_amp=True, opt_ph=True)
    hist_inc_test, amph_inc_test = get_template_distr(cuts_test, atoms, epoch_s, data, draw=draw_pictures, mmp=mmp,
                                                      type='test_incor', opt_amp=opt_amp, opt_ph=opt_ph)
    hist_inc_learn, amph_inc_learn = get_template_distr(cuts_learn, atoms, epoch_s, data, draw=draw_pictures, mmp=mmp,
                                                        type='train_incor', opt_amp=opt_amp, opt_ph=opt_ph)
    hist_corr_train, amph_corr_train = get_template_distr(cuts_cor_train, atoms, epoch_s, data, draw=draw_pictures,
                                                          mmp=mmp, type='train_cor', opt_amp=opt_amp, opt_ph=opt_ph)
    hist_corr_test, amph_corr_test = get_template_distr(cuts_cor_test, atoms, epoch_s, data, draw=draw_pictures,
                                                        mmp=mmp, type='test_cor', opt_amp=opt_amp, opt_ph=opt_ph)

    figsize = (6, 6)
    fig = pb.figure(figsize=figsize, dpi=300)
    # ~ ax = fig.add_subplot(111, projection='3d')
    X, Y = (5, 3)
    hfig = pb.figure(figsize=figsize, dpi=300)
    hpfig = pb.figure(figsize=figsize, dpi=300)
    mark = [7, 4, 5, 6, 'o', 'D', 'h', 'H', '_', '8', 'p', ',', '+', '.', 's', '*', 'd', 3, 0, 1, 2, '1', '3', '4', '2',
            'v', '<', '>', '^']
    atoms_to_use = []
    for m, at in zip(mark[:NATOMS], xrange(NATOMS)):  # ,
        ax = fig.add_subplot(Y, X, at + 1)
        hax = hfig.add_subplot(Y, X, at + 1)
        hpax = hpfig.add_subplot(Y, X, at + 1)
        # ~ sys.exit()
        # ~ pb.figure()
        # ~ pb.title('testing, atom {}'.format(at))
        hax.set_title('Atom {}'.format(at))
        hpax.set_title('Atom {}'.format(at))

        ax.scatter(amph_corr_test[:, 0, at], amph_corr_test[:, 1, at],
                   color='b')  # ,label=u'Testowe poprawne, atom {}'.format(at))
        ax.scatter(amph_inc_test[:, 0, at], amph_inc_test[:, 1, at],
                   color='r', )  # label=u'Testowe błędne, atom'.format(at))

        hax.hist(np.concatenate((amph_inc_test[:, 0, at], amph_inc_learn[:, 0, at])), color='r', normed=True,
                 alpha=0.5, )  # ,label=u'Testowe poprawne, atom {}'.format(at))
        hax.hist(np.concatenate((amph_corr_test[:, 0, at], amph_corr_train[:, 0, at])), color='b', alpha=0.3,
                 normed=True, )  # label=u'Testowe błędne, atom'.format(at))
        hpax.hist(np.concatenate((amph_inc_test[:, 1, at], amph_inc_learn[:, 1, at])), color='r', normed=True,
                  alpha=0.5, )  # ,label=u'Testowe poprawne, atom {}'.format(at))
        hpax.hist(np.concatenate((amph_corr_test[:, 1, at], amph_corr_train[:, 1, at])), color='b', alpha=0.3,
                  normed=True, )  # label=u'Testowe błędne, atom'.format(at))

        print('Atom {}, amplitudes, {} F-Value, {} p, ANOVA'.format(at, *st.f_oneway(
            np.concatenate((amph_inc_test[:, 0, at], amph_inc_learn[:, 0, at])),
            np.concatenate((amph_corr_test[:, 0, at], amph_corr_train[:, 0, at])))))
        print('Atom {}, Phases, {} F-Value, {} p, ANOVA'.format(at, *st.f_oneway(
            np.concatenate((amph_inc_test[:, 1, at], amph_inc_learn[:, 1, at])),
            np.concatenate((amph_corr_test[:, 1, at], amph_corr_train[:, 1, at])))))
        Hvalue, pvalue = st.kruskal(np.concatenate((amph_inc_test[:, 0, at], amph_inc_learn[:, 0, at])),
                                    np.concatenate((amph_corr_test[:, 0, at], amph_corr_train[:, 0, at])))
        if pvalue < 0.1:
            atoms_to_use.append(at)

        print('Atom {}, amplitudes, {} H-Value, {} p, kruskal'.format(at, Hvalue, pvalue))
        # ~ print('Atom {}, Phases, {} H-Value, {} p, kruskal'.format(at, *st.kruskal(np.concatenate((amph_inc_test[:,1, at], amph_inc_learn[:,1, at])), np.concatenate((amph_corr_test[:,1, at], amph_corr_train[:,1, at])))))
        print('\n')

        # ~ ax.scatter(amph_corr_test[:,0, at]**2, amph_corr_test[:, 1, at], color='b',  marker=m,label='test correct trials atom {}'.format(at))
        # ~ ax.scatter(amph_inc_test[:,0, at]**2, amph_inc_test[:, 1,at], color='r', marker=m, label='test incorrect trials, atom {}'.format(at))

        ax.set_xlabel(r'Amplituda, [$\mu$V]')
        hax.set_xlabel(r'Amplituda, [$\mu$V]')
        hpax.set_xlabel(r'Faza')
        hax.set_xlim([-15, 15])
        ax.set_ylabel(r'Faza')
        ax.set_title('Atom {}'.format(at))
    if not atoms_to_use:
        atoms_to_use = list(xrange(NATOMS))
        # ~ ax.set_title('testing, atom {}'.format(at))
    #fig.tight_layout()
    #hfig.tight_layout()
    #fig.tight_layout(pad=10)
    #hfig.tight_layout(pad=10)
    #hpfig.tight_layout(pad=10)
    results_folder = os.path.expanduser("~/results/classifier_results_{}_{}_{}".format(mmp, chnls, os.path.basename(filename)))
    try:
        os.makedirs(results_folder)
    except:
        pass
    fig.savefig(os.path.join(results_folder, 'testing.png'), dpi=300)
    hfig.savefig(os.path.join(results_folder, 'razem_hist.png'), dpi=300)
    hpfig.savefig(os.path.join(results_folder, 'razem_hist_faze.png'), dpi=300)
    ax.legend()
    hax.legend()

    fig = pb.figure(figsize=figsize, dpi=300)
    hfig = pb.figure(figsize=figsize, dpi=300)
    hpfig = pb.figure(figsize=figsize, dpi=300)
    for m, at in zip(mark[:NATOMS], xrange(NATOMS)):  # ,
        hax = hfig.add_subplot(Y, X, at + 1)
        hpax = hpfig.add_subplot(Y, X, at + 1)
        hax.hist(amph_inc_learn[:, 0, at], color='r', alpha=0.5,
                 normed=True)  # label=u'Uczące błędne, atom {}'.format(at))
        hax.hist(amph_corr_train[:, 0, at], color='b', alpha=0.3,
                 normed=True)  # ,label=u'Uczące poprawne, atom {}'.format(at))
        hpax.hist(amph_inc_learn[:, 1, at], color='r', alpha=0.5,
                  normed=True)  # label=u'Uczące błędne, atom {}'.format(at))
        hpax.hist(amph_corr_train[:, 1, at], color='b', alpha=0.3,
                  normed=True)  # ,label=u'Uczące poprawne, atom {}'.format(at))
        hpax.set_title('Atom {}'.format(at))
        hax.set_title('Atom {}'.format(at))
        hax.set_xlabel(r'Amplituda, [$\mu$V]')
        hpax.set_xlabel(r'Faza')
        hpax.set_xticks([-1.6, 0, 1.6])
        ax = fig.add_subplot(Y, X, at + 1)
        # ~ sys.exit()
        # ~ pb.figure()
        # ~ pb.title('training, atom {}'.format(at))
        # ~ ax.scatter(amph_inc_learn[:,0, at]**2, amph_inc_learn[:, 1,at], amph_inc_learn[:, 2,at],  marker='v', color='r', label='test incorrect trials, atom {}'.format(at))
        # ~ ax.scatter(amph_corr_train[:,0, at]**2, amph_corr_train[:, 1, at], amph_corr_train[:, 2, at],  marker = 'o', color='b', alpha=0.5,label='test correct trials atom {}'.format(at))
        # ~ ax.scatter(amph_inc_learn[:,0, at]**2, amph_inc_learn[:, 1,at],  marker=m, color='r', label='test incorrect trials, atom {}'.format(at))
        # ~ ax.scatter(amph_corr_train[:,0, at]**2, amph_corr_train[:, 1, at], marker = m, color='b', alpha=0.5,label='test correct trials atom {}'.format(at))
        ax.scatter(amph_corr_train[:, 0, at], amph_corr_train[:, 1, at], color='b',
                   alpha=0.5, )  # label=u'Uczące poprawne, atom {}'.format(at))
        ax.scatter(amph_inc_learn[:, 0, at], amph_inc_learn[:, 1, at],
                   color='r', )  # label=u'Uczące błędne, atom {}'.format(at))
        ax.set_title('Atom {}'.format(at))
        ax.set_xlabel(r'Amplituda, [$\mu$V]')
        ax.set_ylabel(r'Faza$')
        hax.set_xlim([-15, 15])

        # ~ ax.set_title('training, atom {}'.format(at))
    ax.legend()

    hax.legend()
    #fig.tight_layout(pad=10)
    #hfig.tight_layout(pad=10)
    #hpfig.tight_layout(pad=10)
    fig.savefig(os.path.join(results_folder, 'training.png'), dpi=300)
    hfig.savefig(os.path.join(results_folder, 'training_hist.png'), dpi=300)
    hpfig.savefig(os.path.join(results_folder, 'training_hist_faza.png'), dpi=300)
    # ~ pb.show()
    print(cuts_cor_train.shape)
    fig = pb.figure(figsize=(7, 7), dpi=300)
    ax = fig.add_subplot(111)

    # ~ ax.plot(*siec_tries_timeline(cuts_cor_train, cuts_learn, cuts_cor, cuts_incor), color = 'blue', ls='-',label=u'Sieć dziedzina czasu')
    # ~ print('net timeline\n\n')
    # ~ ax.plot(*siec_tries(amph_corr_train, amph_inc_learn, np.concatenate((amph_corr_test, amph_corr_train)), np.concatenate((amph_inc_test, amph_inc_learn)),atoms_to_use=atoms_to_use), color = 'green', ls='-',label=u'Sieć dziedzina atomów')
    # ~ print('net_atoms \n\n')
    print('lda_atoms\n\n')
    FPR, TPR, roc_integral = lda_test(amph_corr_train[:, 0, atoms_to_use], amph_inc_learn[:, 0, atoms_to_use],
                                      np.concatenate(
                                          (amph_corr_test[:, 0, atoms_to_use], amph_corr_train[:, 0, atoms_to_use])),
                                      np.concatenate(
                                          (amph_inc_test[:, 0, atoms_to_use], amph_inc_learn[:, 0, atoms_to_use])))
    ax.plot(FPR, TPR,
            color='red', ls='-', label=u'LDA, amplitudy atomów, ROC: {}'.format(roc_integral))





    print('bayes 1 dobry atom\n\n')
    FPR, TPR, roc_integral = bayes_class(amph_corr_train[:, 0, atoms_to_use[0]], amph_inc_learn[:, 0, atoms_to_use[0]], np.concatenate(
        (amph_corr_test[:, 0, atoms_to_use[0]], amph_corr_train[:, 0, atoms_to_use[0]])),
                         np.concatenate((amph_inc_test[:, 0, atoms_to_use[0]], amph_inc_learn[:, 0, atoms_to_use[0]])))
    ax.plot(FPR, TPR,
            color='yellow', ls=':', label=u'Bayes, amplitudy pierwszego atomu, ROC: {}'.format(roc_integral))

    print('bayes il skalarny\n\n')
    FPR, TPR, roc_integral = bayes_class(hist_corr_train, hist_inc_learn, np.concatenate((hist_corr_test, hist_corr_train)),
                np.concatenate((hist_inc_test, hist_inc_learn)))
    ax.plot(FPR, TPR, color='black', ls=':',
            label=u'Bayes, iloczyn skalarny wycinka ze wzorcem,  ROC: {}'.format(roc_integral))
    ax.plot(np.linspace(0, 1, 100), np.linspace(0, 1, 100), color='black', label=u'X=Y')

    FPR, TPR, roc_integral = bayes_class_multi(amph_corr_train, amph_inc_learn, amph_corr_test, amph_inc_test, atoms_to_use)
    ax.plot(FPR, TPR,
            label=u'Bayes, amplitudy atomów, ROC {}'.format(roc_integral))
    ax.set_title('{} {}'.format(chnls, filename))

    ax.legend(loc=4, prop={'size': 6})
    fig.savefig(os.path.join(results_folder, 'rocs.png'), dpi=300)

    return [[hist_inc_learn, hist_corr_train, hist_inc_test, hist_corr_test],
            [amph_corr_train, amph_inc_learn, amph_corr_test, amph_inc_test]]


def cuts_matching_pursuit(cuts, chnls, type='incor', mmp=False):
    """

    :param cuts: mne epochs
    :param chnls: list[str], channels to extract matching pursuit from
    :param type: string - name of the stimulation type
    :param mmp: do multivariate matching pursuit?
    :return: path to result book
    """
    if not mmp:
        mmp = 'SMP'
    else:
        mmp = mmp

    cuts = cuts.pick_channels(chnls)
    chnls = cuts.ch_names
    print(chnls)
    # sys.exit()
    old_cwd = os.getcwd()

    cuts_data = cuts.get_data()*1e6

    N_realisations = len(cuts_data)
    len_cut = len(cuts_data[0][0])
    cuts_arr = np.array(cuts_data)
    dataFileName = os.path.basename(cuts.info["filename"])

    mp_cache_path = './' + os.path.basename(cuts.info["filename"]) + '_mp/mp_cache/'
    mp_out_path = '../mp_output/'
    os.chdir(os.path.dirname(cuts.info["filename"]))

    if not os.path.exists(mp_cache_path):
        os.makedirs(mp_cache_path)
    os.chdir(mp_cache_path)
    if not os.path.exists(mp_out_path):
        os.makedirs(mp_out_path)

    # ~os.chdir(os.path.dirname(self.filename)+mp_cache_path[1:])
    print(os.getcwd())
    for nr, chnl in enumerate(chnls):
        cache_mp_filename = 'mp_cache_' + 'type_{}_chnl_{}'.format(type, chnl) + '.bin'
        flatten = np.resize(cuts_arr[:, nr, :].T, (len_cut * N_realisations, 1))

        flatten.astype(np.float32).tofile(cache_mp_filename)
        mp_config = '''# OBLIGATORY PARAMETERS
nameOfDataFile         {}
nameOfOutputDirectory  {}
writingMode            CREATE
samplingFrequency      {}
numberOfChannels       {}
selectedChannels       1-{}
numberOfSamplesInEpoch {}
selectedEpochs         1
typeOfDictionary       OCTAVE_STOCH
energyError         0.1 90.00
randomSeed             auto
reinitDictionary       NO_REINIT_AT_ALL
maximalNumberOfIterations {}
energyPercent             95.0
MP                        {}
scaleToPeriodFactor       1.0
pointsPerMicrovolt        1.0

# ADDITIONAL PARAMETERS

diracInDictionary         NO
gaussInDictionary         NO
sinCosInDictionary        NO
gaborInDictionary         YES
progressBar               ON
'''.format(cache_mp_filename, mp_out_path, cuts.info["sfreq"], int(N_realisations), int(N_realisations), len_cut, NATOMS, mmp)
        config_filename = 'mp_config.txt'
        configfile = open(config_filename, 'w')
        configfile.write(mp_config)
        configfile.close()

        os.system('pwd')
        print(chnl)
        comm = "mp5-amd64 -f {}".format(config_filename)
        print(comm)
        os.system(comm)
    os.chdir(old_cwd)
    return '/' + mp_cache_path + '/' + mp_out_path + cache_mp_filename
