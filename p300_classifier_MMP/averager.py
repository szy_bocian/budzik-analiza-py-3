# coding: utf-8
import mne
import numpy as np
from mne.epochs import BaseEpochs


def averager(data, classes, N=3):
    assert isinstance(data, np.ndarray)
    new_data = []
    new_classes = []
    for i in np.unique(classes):
        to_avr = data[classes==i]
        for j in range(0, len(to_avr), N):
            try:
                avr = np.mean(to_avr[j:j+N], axis=0, keepdims=True)
                new_data.append(avr)
                new_classes.append(i)
            except IndexError:
                break

    new_data = np.vstack(new_data)
    new_classes = np.hstack(new_classes)
    print "new_data, after averaging", new_data.shape
    print "new_classes, after averaging", new_classes.shape
    return new_data, new_classes

def averager_epochs(epochs, classes, N=3):
    assert isinstance(epochs, _BaseEpochs)
    info = epochs.info
    data = epochs.get_data()

    new_data = []
    new_classes = []
    for i in np.unique(classes):
        to_avr = data[classes==i]
        for j in range(0, len(to_avr), N):
            try:
                data_to_avr = to_avr[j:j + N]


                if data_to_avr.shape[0] != N:  # w bloku do uśrednienia jest inna ilośc epoch niż N (np na końcu)
                    continue  # to nie bierzemy tego bloku

                avr = np.mean(data_to_avr, axis=0, keepdims=True)
                new_data.append(avr)
                new_classes.append(i)
            except IndexError:
                break

    new_data = np.vstack(new_data)
    new_classes = np.hstack(new_classes)



    events = np.ones((len(new_classes), 3,), dtype=int)
    for i in xrange(len(new_classes)):
        events[i, 0] = i * len(epochs.times) * 2
        events[i, 1] = 1
        events[i, 2] = new_classes[i]

    new_data = mne.epochs.EpochsArray(new_data, info, tmin=epochs.tmin, events=events, event_id=epochs.event_id)

    print "new_data, after averaging", new_data
    print "new_classes, after averaging", new_classes.shape


    return new_data, new_classes
