#!/usr/bin/python2
# coding: utf-8
import numpy as np
import scipy.stats as st
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib as mpl


# Make data.
rn = np.arange(1, 61)
N1, N2 = np.meshgrid(rn, rn)

print N1
print N2

# poziom p (a właściwie alfa)
p = 0.05

U = np.loadtxt("mww-U-p{}.dat".format(p))

AUC = 1 - U/(N1*N2)

AUC_0 = np.zeros(np.array(AUC.shape)+1)
AUC_0[1:, 1:] = AUC
# AUC_0[0, :] = AUC_0[:, 0] = np.nan

fig = plt.figure(figsize=(4,4), dpi=600)
im = plt.imshow(AUC_0, cmap="binary", vmin = 0.5, vmax = 1.0, interpolation = "nearest", origin = "lower")
cbar = fig.colorbar(im, shrink=0.5, aspect=5, ticks=[0.5, 0.6, 0.7, .8, .9, 1])#np.linspace(0.5, 1, 5))
cbar.set_label('AUC')
plt.xlabel("n1")
plt.ylabel("n2")
fig.savefig('AUC_crit_fancy.png', dpi=600)


# AUC[N1 > N2] = np.nan
fig = plt.figure(figsize=(8,4), dpi=600)
ax = fig.gca(projection='3d')

# special colormap
colors = [(0, 0, 0, 0)]
colors.extend(cm.binary(np.linspace(0, 1, 1000)))
cmap = mpl.colors.ListedColormap(colors)
cmap.set_bad('black')

surf = ax.plot_surface(N1, N2, AUC, cmap=cmap, linewidth=0, antialiased=False, vmin = 0.5, vmax = 1, rstride = 1, cstride = 1, shade=True)
ax.plot_wireframe(N1, N2, AUC, antialiased=True, rstride = 10, cstride = 10, color='black')

cbar = fig.colorbar(surf, shrink=0.5, aspect=4, ticks=[0.5, 0.6, 0.7, .8, .9, 1])
cbar.set_label('AUC')
ax.view_init(25, 30)
fig.savefig('AUC_crit_fancy_3d.png', dpi=600)

plt.show()




meanrank = rn**2 / 2
sd = np.sqrt(rn**2 * (2*rn+1) / 12.0)  # bez tiecorrection, ale powinno być ok

z = st.norm.ppf(1-p)  # jednostronnie
u = z*sd + meanrank + 0.5
print u

auc_2 = u / rn**2
auc_1 = AUC[rn-1, rn-1]


plt.figure()
plt.subplot(311)
plt.title(u"AUC krytyczny obliczony dwiema metodami: 1 - dokładna, 2 - przybliżona rozk. norm., p = 0.05")
plt.plot([0, 61], [1, 1], 'tab:gray', linewidth = 1)
plt.plot(rn, auc_1, label = "AUC_1")
plt.plot(rn, auc_2, label = "AUC_2")
plt.xlim(0, 61)
plt.ylabel("AUC")
plt.legend()

plt.subplot(312)
plt.plot([0, 61], [0, 0], 'tab:gray', linewidth = 1)
plt.plot(rn, (auc_1-auc_2), label = u"różnica")
plt.xlim(0, 61)
plt.ylabel("AUC_1 - AUC_2")
plt.legend()

plt.subplot(313)
plt.plot([0, 61], [0, 0], 'tab:gray', linewidth = 1)
plt.plot(rn, (auc_1-auc_2), label = u"różnica")
plt.ylim(-0.004, 0.004)
plt.xlim(0, 61)
plt.ylabel("AUC_1 - AUC_2")
plt.xlabel(u"n (równoliczne grupy)")
plt.legend()






plt.show()
