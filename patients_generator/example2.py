# Example 2

import numpy as np
import matplotlib.pyplot as plt
import connectivipy as cp
from connectivipy import mvar_gen

"""
In this example we don't use Data class
"""

fs = 256.
acf = np.zeros((3, 3, 3))
# matrix shape meaning
# (p,k,k) k - number of channels,
# p - order of mvar parameters

acf[0, 0, 0] = 0.3
acf[0, 1, 0] = 0.6
acf[1, 0, 0] = 0.1
acf[1, 1, 1] = 0.2
acf[1, 2, 0] = 0.6
acf[2, 2, 2] = 0.2
acf[2, 1, 0] = 0.4

# generate 3-channel signal from matrix above
y = mvar_gen(acf, int(1e3))
print y.shape

plt.figure()
for i,s in enumerate(y):
    plt.subplot(3,1,i+1)
    plt.plot(s)
plt.figure()

# assign static class cp.Mvar to variable mv
mv = cp.Mvar

# find best model order using Vieira-Morf algorithm

best, crit = mv.order_akaike(y, 15, 'yw')
plt.plot(1+np.arange(len(crit)), crit, 'g')

print(best)
# here we know that this is 3 but in real-life cases
# we are always uncertain about it

# now let's fit parameters to the signal
best = 3
av, vf = mv.fit(y, best, 'yw')

chn, N = y.shape
print N * np.log(np.linalg.det(vf)) + 2. * ((best + 1) * chn * (1 + chn))
print N * np.log(np.linalg.det(vf))
print 2 * ((best + 1) * chn * (1 + chn))
# plt.show()
# exit()
print acf
np.set_printoptions(precision=3, suppress = True)
print av


y = mvar_gen(av, int(5e4))
plt.figure()
for i,s in enumerate(y):
    plt.subplot(3,1,i+1)
    plt.plot(s)
plt.show()
exit()
# and check whether values are correct +/- 0.01
print(np.allclose(acf, av, 0.01, 0.01))


# now we can calculate Directed Transfer Function from the data
dtf = cp.conn.DTF()
dtfval = dtf.calculate(av, vf, 128)
# all possible methods are visible in that dictionary:
print(cp.conn.conn_estim_dc.keys())



plt.figure()
cp.plot_conn(dtfval, 'DTF values', fs)


plt.show()