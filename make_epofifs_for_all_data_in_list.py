#!/usr/bin/python
# coding: utf8

import os
import sys
from config import tour_dates
from miscellaneous import n_parent_dirpath

from p300_czuciowe_make_epofifs import p300_czuciowe
from p300_sluchowe_slowa_make_epofifs import p300_slowa
from p300_sluchowe_tony_make_epofifs import p300_tony
from p300_wzrokowe_make_epofifs import p300_wzrokowe
from global_local_make_epofifs import global_local

list_of_png_filepaths = open(sys.argv[1], "r")
data_dir = sys.argv[2]


list_of_filepaths = []
for path in list_of_png_filepaths:
    if path.strip():
        png_path = os.path.basename(os.path.abspath(path))[8:]
        obci_pos = png_path.index(".obci")
        raw_name = png_path[:obci_pos] + ".obci.raw"
        
        cat_path = os.path.dirname(os.path.abspath(path))

        tour_name = os.path.basename(n_parent_dirpath(cat_path, 2))
        patient = os.path.basename(n_parent_dirpath(cat_path, 3))
        paradigm = os.path.basename(n_parent_dirpath(cat_path, 4))

        dates = tour_dates[tour_name]

        for d in dates:
            raw_path = os.path.join(data_dir, d, paradigm, patient, raw_name)
            if os.path.exists(raw_path):
                list_of_filepaths.append(raw_path)

list_of_filepaths = sorted(set(list_of_filepaths))  # usunięcie powtórzeń i posortowanie

del cat_path, dates, d, raw_path, png_path, obci_pos, raw_name, tour_name, patient, paradigm

i = start = 0
fully_automatic = True

list_of_filepaths = list_of_filepaths[start:]

# dotąd doszedłem i go nie dokończyłem:
# 4 /media/reszta/BUDZIKdane/2016-07/global-local/WS/weronika_Global-local3_2016_Aug_03_1110.obci.raw

for path in list_of_filepaths:  # przeglądamy rekursywnie wszystkie pliki w katalogu podanym, jako pierwszy argument w linii komend
    if path:  # w pliku mogą być puste linijki, więc je pomijamy
        # zakładam, że trzymamy się przyjętej konwencji i nazwy katalogów są takie, jak prefiksy w nazwach skryptów
        # a hierarchia jest taka, że katalog paradygmatu jest nadrzędny względem katalogu pacjenta
        paradigm = os.path.basename(n_parent_dirpath(path, 2)).replace("-", "_")
        
        print "\n==============================================\n==============================================\nANALIZOWANY PONOWNIE PLIK:"
        print i, path
        print

        try:  # uruchomienie, jako osobnego procesu skryptu właściwego dla pliku, do którego prowadzi ścieżka path
            make_epofifs = eval(paradigm)
            make_epofifs((path,), fully_automatic = fully_automatic)
        except (NameError, TypeError):
            print 'Nie można uruchomić polecenia:  {}(("{}",), fully_automatic = {)\nPrzechodzę dalej.'.format(paradigm, path, fully_automatic)
        
        i += 1