import numpy
import pylab
from .corrections import Correction
from .power import NoValidEpochsException, PowerData
from .stats import Statistic


def generate_image(power_data_by_name: dict, path_for_image: str, t_ref: float, t_move: float,
                   alpha_level = 0.05, caption = None,
                   statistic = None,
                   correction = None):
    image_cols = len(power_data_by_name)
    image_rows = 2 if statistic is None else 3
    figure = pylab.figure(figsize=(image_cols * 8, image_rows * 6))
    if caption is not None:
        pylab.figtext(.02, .03, caption)
    subplot_index = 0
    for fun_label, power_data in power_data_by_name.items():
        subplot_index += 1
        plot_ylabel = (subplot_index == 1)
        try:
            title = 'ERDS of %s (%d trials)' % (fun_label, len(power_data.values))

            # preparing data
            reference_bins = numpy.sum(power_data.arr_t < t_ref)

            # computing mean spectrogram with max value to 1
            mean_abs_spectrogram = numpy.nanmean(power_data.values, axis=0)
            abs_spectrum = mean_abs_spectrogram / numpy.nanmax(mean_abs_spectrogram)

            pylab.subplot(image_rows, image_cols, subplot_index)
            plot_spectrogram_data(power_data.arr_f, power_data.arr_t, abs_spectrum,
                                  plot_ylabel=plot_ylabel, title=title, t_lines=[t_ref, t_move])

            # computing relative spectrogram
            reference_for_spectrogram = numpy.nanmean(mean_abs_spectrogram[:,0:reference_bins], axis=1, keepdims=True)
            mean_rel_spectrogram = 10 * numpy.log10(mean_abs_spectrogram / reference_for_spectrogram)

            pylab.subplot(image_rows, image_cols, subplot_index + image_cols)
            plot_spectrogram_data(power_data.arr_f, power_data.arr_t, mean_rel_spectrogram,
                                  limit_values=True,
                                  plot_xlabel=(image_rows==2), plot_ylabel=plot_ylabel, title=title, t_lines=[t_ref, t_move])

            if statistic is not None:
                # computing p-values with statistical test
                refs_data = power_data.values[:,:,0:reference_bins].transpose((0,2,1)).reshape((len(power_data.values)*reference_bins, len(power_data.arr_f), 1))
                p_values = statistic.compute_p_values(power_data.values, refs_data)
                if correction is not None:
                    alpha_level = correction.compute_alpha_level(alpha_level, p_values.flatten())

                rel_spectrum = numpy.where(p_values <= alpha_level, mean_rel_spectrogram, numpy.nan)

                pylab.subplot(image_rows, image_cols, subplot_index + 2 * image_cols)
                plot_spectrogram_data(power_data.arr_f, power_data.arr_t, rel_spectrum,
                                      limit_values=True, plot_xlabel=True, plot_ylabel=plot_ylabel, title=title, t_lines=[t_ref, t_move])

        except NoValidEpochsException:
            pass

    pylab.savefig(path_for_image)
    pylab.close(figure)


def plot_spectrogram_data(arr_f, arr_t, data, limit_values=False, plot_xlabel=False, plot_ylabel=False, title=None, t_lines=[]):
    options = {'vmin': -10, 'vmax': 10} if limit_values else {}
    pylab.imshow(data, aspect='auto', interpolation='none', origin='lower',
                 cmap='jet', extent=(arr_t[0], arr_t[-1], arr_f[0], arr_f[-1]), **options)
    if title is not None:
        pylab.title(title)
    for t in t_lines:
        pylab.vlines(t, arr_f[0], arr_f[-1])
    if plot_xlabel:
        pylab.xlabel('time [s]')
    if plot_ylabel:
        pylab.ylabel('frequency [Hz]')
    pylab.colorbar()
