import abc
import collections
import numpy
import pylab
import scipy
from xml.etree import ElementTree


LabeledEpoch = collections.namedtuple('LabeledEpoch', ('epoch', 'label'))


def invert_list(l):
    return {l[i]: i for i in range(len(l))}


class Filterable:

    def __init__(self, freq_sampling):
        self.freq_sampling = freq_sampling
        self.freq_nyquist = 0.5 * freq_sampling

    def _normalize_frequency(self, x):
        return x / self.freq_nyquist if numpy.isscalar(x) else list(map(self._normalize_frequency, x))

    def filtered(self, fpass, fstop, gpass, gstop, ftype, plot=False):
        wpass = self._normalize_frequency(fpass)
        wstop = self._normalize_frequency(fstop)
        b, a = scipy.signal.iirdesign(wpass, wstop, gpass, gstop, ftype=ftype)
        if plot:
            w, h = scipy.signal.freqz(b, a, 1000)
            pylab.plot(self.freq_nyquist * w / numpy.pi, 40 * numpy.log10(abs(h)))  # 40 bo filtfilt
            pylab.ylabel('response [dB]')
            pylab.xlabel('frequency [Hz]')
            pylab.ylim(-50, 5)
            pylab.show()
        return self._filtered(b, a)

    @abc.abstractmethod
    def _filtered(self, b, a):
        return ...

    @abc.abstractmethod
    def clone(self):
        return ...


class Channel(Filterable):

    def __init__(self, data, freq_sampling):
        assert len(data.shape) == 1
        super().__init__(freq_sampling)
        self.data = data

    def clone(self):
        return Channel(numpy.array(self.data, copy=True), self.freq_sampling)

    def _filtered(self, b, a):
        return Channel(scipy.signal.filtfilt(b, a, self.data), self.freq_sampling)

    def get_epochs(self, path_to_tag: str, tmin: float, tmax: float):
        epochs = []
        xml_tags = ElementTree.parse(path_to_tag).getroot()
        for tag in xml_tags.iter('tag'):
            name = tag.attrib['name'].lower()
            position = float(tag.attrib['position'])
            offset_start = int(self.freq_sampling * (position + tmin))
            offset_end = int(self.freq_sampling * (position + tmax))
            if 0 <= offset_start and offset_end < len(self.data):
                epochs.append(LabeledEpoch(
                    Channel(self.data[offset_start:offset_end], self.freq_sampling),
                    name
                ))
        return epochs


class Signal(Filterable):

    def __init__(self, data, freq_sampling, channel_names):
        assert len(data.shape) == 2
        assert data.shape[0] == len(channel_names)
        super().__init__(freq_sampling)
        self.data = data
        self.channel_name_from_index = channel_names
        self.channel_count, self.sample_count = data.shape
        self.channel_index_from_name = invert_list(channel_names)

    def clone(self):
        return Signal(numpy.array(self.data, copy=True), self.freq_sampling, self.channel_name_from_index)

    def _filtered(self, b, a):
        return Signal(scipy.signal.filtfilt(b, a, self.data, axis=1), self.freq_sampling, self.channel_name_from_index)

    @classmethod
    def read(cls, path_to_raw, path_to_xml):
        RS = '{http://signalml.org/rawsignal}'
        xml_spec = ElementTree.parse(path_to_xml).getroot()
        sfreq = int(float(xml_spec.find(RS + 'samplingFrequency').text))
        channel_count = int(xml_spec.find(RS + 'channelCount').text)
        sample_count = int(xml_spec.find(RS + 'sampleCount').text)
        sample_type = xml_spec.find(RS + 'sampleType').text

        channel_names = list(map(
            lambda tag: tag.text,
            xml_spec.iter(RS + 'label')
        ))

        gains = numpy.array(list(map(
            lambda tag: float(tag.text),
            xml_spec.find(RS + 'calibrationGain').iter(
                RS + 'calibrationParam')
        )))

        dtype = 'f8' if sample_type == 'DOUBLE' else 'f4'
        data = numpy.fromfile(path_to_raw, dtype=dtype).reshape(
            [sample_count, channel_count]
        ) * gains  # resulting in µV

        return Signal(data.T, sfreq, channel_names)

    def decimate(self, freq_sampling):
        while self.freq_sampling > freq_sampling:
            self.data = scipy.signal.decimate(self.data, q=2, ftype='fir', zero_phase=True, axis=1)
            self.freq_sampling //= 2
            self.freq_nyquist //= 2

    def montage(self, weights):
        w = numpy.zeros(self.channel_count)
        result = numpy.zeros(self.data.shape[1])
        for channel, weight in weights.items():
            result += weight * self.data[self.channel_index_from_name[channel]]
        return Channel(result, self.freq_sampling)

    def get_epochs(self, path_to_tag: str, tmin: float, tmax: float):
        epochs = []
        xml_tags = ElementTree.parse(path_to_tag).getroot()
        for tag in xml_tags.iter('tag'):
            name = tag.attrib['name'].lower()
            position = float(tag.attrib['position'])
            offset_start = int(self.freq_sampling * (position + tmin))
            offset_end = int(self.freq_sampling * (position + tmax))
            if 0 <= offset_start and offset_end < self.data.shape[1]:
                epochs.append(LabeledEpoch(
                    Signal(self.data[:, offset_start:offset_end], self.freq_sampling, self.channel_name_from_index),
                    name
                ))
        return epochs
