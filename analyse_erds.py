#!/usr/bin/env python3
import matplotlib
matplotlib.use("Agg")

import argparse
import numpy
import os.path
import subprocess
import sys
import traceback
from collections import OrderedDict

from config import create_outdir_name
from erds.artifacts import ArtifactMethod, ArtifactMethodFromTags
from erds.classifiers import Classifier
from erds.corrections import Correction
from erds.io import Signal
from erds.plotting import generate_image
from erds.power import PowerExtractor
from erds.stats import Statistic, StatisticBootstrap


DEFAULT_WINDOW = 'hanning'
RESAMPLING_FREQUENCY = 128.0
FILTER_PARAMS = ([2.0, 30.0], [1.0, 35.0], 1.0, 15.0, 'butter')
T_MIN = -2.5
T_REF = -1.0
T_MOVE = 0.0
T_MAX = +8.5
F_MAX = 25.0
TAGS = {
    'reka': [
        'erds_instr1.wav',
        'reka_razem',
        'reka_pacjent',
    ],
    'noga': [
        'erds_instr2.wav',
        'noga_razem',
        'noga_pacjent',
    ]
}


def main():
    artifact_methods = ArtifactMethod.choices()
    classifiers = Classifier.choices()
    corrections = Correction.choices()
    statistics = Statistic.choices()

    parser = argparse.ArgumentParser(description='Analyses EEG data in ERDS paradigm.')
    parser.add_argument('files', nargs='+', metavar='file', help='path to *.raw data file')
    parser.add_argument('-a', dest='artifact_method', default='none', choices=artifact_methods,
                        help='artifact reduction method (default: none)')
    parser.add_argument('-c', dest='classifier', default='none', choices=classifiers,
                        help='classifier for cross-validation (default: none)')
    parser.add_argument('-m', dest='correction', default='none', choices=corrections,
                        help='statistical correction for multiple comparisons (default: none)')
    parser.add_argument('-r', dest='repeats', default=None, type=int,
                        help='number of repeats for bootstrap method (default: '+str(StatisticBootstrap.DEFAULT_REPEATS)+')')
    parser.add_argument('-s', dest='statistic', default='none', choices=statistics,
                        help='type of statistical test (default: none)')
    parser.add_argument('-u', dest='use_welch', action='store_const', const=True, default=False,
                        help='use Welch method for power estimation')
    parser.add_argument('-w', dest='window', default=DEFAULT_WINDOW,
                        help='window function for spectrogram (default: '+DEFAULT_WINDOW+')')
    namespace = parser.parse_args()

    artifact_method = artifact_methods[namespace.artifact_method]
    classifier = classifiers[namespace.classifier]
    statistic = statistics[namespace.statistic]
    correction = corrections[namespace.correction]

    if namespace.repeats is not None:
        if isinstance(statistic, StatisticBootstrap):
            statistic.repeats = namespace.repeats
        else:
            sys.stderr.write('WARNING: -r parameter is valid only with -s bootstrap\n')

    for path_to_raw in namespace.files:
        try:
            process(path_to_raw, tags=TAGS,
                    artifact_method=artifact_method, classifier=classifier,
                    statistic=statistic, correction=correction,
                    window=namespace.window, use_welch=namespace.use_welch)
        except Exception as e:
            sys.stderr.write('ERROR: {}\n'.format(e))
            traceback.print_exc()


def process(path_to_raw: str, tags: dict, window: str, use_welch: bool,
            artifact_method,
            classifier,
            statistic,
            correction):
    if path_to_raw[-4:] != '.raw':
        raise Exception("invalid path")
    core_of_path = path_to_raw[:-4]
    core_of_name = os.path.basename(core_of_path)
    path_to_xml = core_of_path + '.xml'
    path_to_tag = core_of_path + '.tag'
    signal = get_preprocessed_signal(path_to_raw, path_to_xml)

    all_features = []
    image_caption = get_git_version() + ' '.join(sys.argv)
    outdir = os.path.expanduser(create_outdir_name(path_to_raw))
    create_output_dir(outdir)
    if isinstance(artifact_method, ArtifactMethodFromTags):
        artifact_method.initialize(path_to_raw)
    if artifact_method is not None:
        signal = artifact_method.replace_artifacts(signal)

    for suffix, tag_names in tags.items():
        path_for_image = os.path.join(outdir, core_of_name + '.' + suffix + '.png')
        power_extractor = PowerExtractor(window=window, use_welch=use_welch)

        power_data_by_name = OrderedDict()
        features = None
        for name, montage in montages():
            power_data = power_extractor.extract(signal, montage, tag_names, path_to_tag, T_MIN, T_MAX, F_MAX)
            power_data_by_name[name] = power_data
            power_features = power_data.values
            #reference_bins = numpy.sum(power_data.arr_t < T_REF)
            #power_features /= numpy.mean(power_features[:,:,:reference_bins], axis=2, keepdims=True)
            power_features = power_features.reshape((power_features.shape[0], -1))
            if features is None:
                features = power_features
            else:
                features = numpy.concatenate((features, power_features), axis=1)
        all_features.append(features)

        generate_image(power_data_by_name, path_for_image, T_REF, T_MOVE,
                       statistic=statistic, correction=correction, caption=image_caption)

    if classifier is not None:
        correctness = classifier.test(*all_features)
        print('%6.2f%% %s' % (100*correctness, os.path.basename(path_to_raw)))


def get_preprocessed_signal(path_to_raw: str, path_to_xml: str):
    signal = Signal.read(path_to_raw, path_to_xml)
    signal.decimate(RESAMPLING_FREQUENCY)
    return signal.filtered(*FILTER_PARAMS)


def montages():
    return [
        ('C3', {'C3': 1.0, 'F3': -0.25, 'P3': -0.25, 'Cz': -0.25, 'T3': -0.25}),
        ('Cz', {'Cz': 1.0, 'Fz': -0.25, 'Pz': -0.25, 'C3': -0.25, 'C4': -0.25}),
        ('C4', {'C4': 1.0, 'F4': -0.25, 'P4': -0.25, 'Cz': -0.25, 'T4': -0.25}),
    ]


def get_git_version():
    try:
        return subprocess.run(['git', 'rev-parse', 'HEAD'],
                              cwd=os.path.dirname(os.path.realpath(__file__)),
                              stdout=subprocess.PIPE
                              ).stdout.decode()
    except:
        return '' # git not available


def create_output_dir(outdir: str):
    try:
        os.makedirs(outdir)
    except OSError as ex:
        if ex.errno != 17:  # do nothing if already exists
            raise


if __name__ == '__main__':
    main()
    sys.exit(0)
