import sys
import numpy as np
import os.path, os
from helper_functions import evoked_list_plot_smart_tags, get_epochs_fromfile_flist, stag_amp_grad_art_rej
from obci.analysis.obci_signal_processing.tags import tags_file_writer as tags_writer
from obci.analysis.obci_signal_processing.tags import tag_utils
from analyse_common import get_clean_epochs, get_filelist
from config import OUTDIR


def savetags(stags, filename, start_offset=0, duration=0.1):
    '''Create tags XML from smart tag list'''
    writer = tags_writer.TagsFileWriter(filename)
    for stag in stags:
        tag = stag.get_tags()[0]
        tag['start_timestamp'] += start_offset
        tag['end_timestamp'] += duration + start_offset
        writer.tag_received(tag)
    writer.finish_saving(0.0)

def sep(filename, *args):
    '''analyzes SEP as of design document'''
    montages = [['custom', 'M1', 'M2'], ['csa']]
    filtr = [[2, 30], [0.1, 33], 3, 12]
    drop_chnls =  [u'l_reka', u'p_reka', u'l_noga', u'p_noga', u'haptic1', u'haptic2',
                    u'phones', u'Driver_Saw']
    start_offset=-0.1
    duration=0.9
    filelist, filename = get_filelist(filename, args)

    for montage in montages:
        suffix = '_{}'.format(montage)

        get_from_file_args = dict(ds='',
                              tags_function_list=[None,],
                              start_offset=start_offset, duration=duration, 
                              filter=filtr, montage=montage,
                              drop_chnls = drop_chnls,
                              tag_name = 'tag',
                              tag_correction_chnls=['haptic1', 'haptic2']
                              )
                              
        clean = get_clean_epochs(get_from_file_args,
                                                filelist,
                                                montage,
                                                '',
                                                [None,],
                                                )
        
        #~ epochs = get_epochs_fromfile_flist(filename[:-4], [None,], start_offset=start_offset, duration=duration, 
                            #~ filter=filtr, montage=montage,
                            #~ drop_chnls = drop_chnls,
                            #~ tag_name = 'tag',
                            #~ tag_correction_chnls=['haptic1', 'haptic2']
                            #~ )[0]
        #~ clean, dirty = stag_amp_grad_art_rej(epochs, 150, 100)
        if not clean:
            raise Exception('All epochs dirty')
            
        try:
            os.mkdir(OUTDIR)
        except Exception:
            pass
        
        fig = evoked_list_plot_smart_tags(clean,
                                          show=False,
                                          chnames=['C3', 'C4', 'Cz'],
                                          size=(20,5),
                                          addline=[0.3],
                                          start_offset=start_offset,)
        savefileimg = os.path.join(OUTDIR, os.path.basename(filename)+suffix+'.png')
        # savefiletag = os.path.join(OUTDIR, os.path.basename(filename)+suffix+'_bad_epochs.tag.xml')
        fig.savefig(savefileimg)
        # savetags(dirty, savefiletag, start_offset, duration)
        

def main():
    try:
        filelist = sys.argv[1:]
    except IndexError:
        raise IOError( 'Please provide path to .raw')
    sep(*filelist)

if __name__ == '__main__':
    main()

